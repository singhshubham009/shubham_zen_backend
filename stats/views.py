# from django.shortcuts import render
from django.http import JsonResponse
import sqlite3
import json
import time


def get_data():
    connection = sqlite3.connect("main.db")

    cursor = connection.cursor()

    cursor.execute("select currency_id, name, symbol,"
                   "rank, price_usd, price_btc,"
                   "volume_usd_24h, market_cap_usd, available_supply,"
                   "total_supply, percent_change_1h, percent_change_24h,"
                   "percent_change_7d, last_updated, max(last_updated)"
                   "from ticker_info group by name order by rank")

    results = cursor.fetchall()
    temp = []
    for items in results:

        temp.append({
                    "id": items[0],
                    "name": items[1],
                    "symbol": items[2],
                    "rank": items[3],
                    "price_usd": items[4],
                    "price_btc": items[5],
                    "volume_usd": items[6],
                    "market_cap_usd": items[7],
                    "available_supply": items[8],
                    "total_supply": items[9],
                    "percent_change_1h": items[10],
                    "percent_change_24h": items[11],
                    "percent_change_7d": items[12],
                    "last_updated": items[13]
                    })
    connection.close()
    return temp


def get_currency_data(currency_id):
    connection = sqlite3.connect("ticker.db_dec20_2017")

    cursor = connection.cursor()
    b=time.time()
    cursor.execute("select price_usd, volume_usd_24h, market_cap_usd, percent_change_1h, percent_change_24h, percent_change_7d, last_updated from ticker_info where currency_id=\'{}\'".format(currency_id))

    results = cursor.fetchall()
    temp = [
            {'monthly': {}},
            {'daily': {}},
            {'weekly': {}}]
    for items in results:
        # import pdb;pdb.set_trace()
        a = b - int(items[6])
        if a // 86400 <= 30:
            temp[0]['monthly'][items[6]]=[items[0],items[1],items[2],items[3],items[4],items[5]]
        if a // 3600 <= 24:
            temp[1]['daily'][items[6]]=[items[0],items[1],items[2],items[3],items[4],items[5]]
        if a // 86400 <= 7:
            temp[2]['weekly'][items[6]]=[items[0],items[1],items[2],items[3],items[4],items[5]]

    connection.close()
    return temp


def content(request):
    if request.method == 'GET':
        content_list = get_data()
        return JsonResponse({'items': content_list}, status=200)
    elif request.method == 'POST':
        req_body = json.loads(request.body.decode('utf-8'))
        currency_name = req_body['currency_id']
        graph = get_currency_data(currency_name)
        return JsonResponse({'items': graph}, status=200)
